﻿using System;

namespace CursoCSharp.Fundamentos
{
    class Inferencia
    {
        public static void Executar()
        {
            var nome = "Leonardo";
            Console.WriteLine(nome);

            var idade = 32;
            Console.WriteLine(idade);

            Console.ReadKey();
        }
    }
}
